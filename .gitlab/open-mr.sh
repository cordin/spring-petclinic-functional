#!/usr/bin/env bash

TARGET_BRANCH=${DEFAULT_BRANCH}
PROJECT_URL=${API_URL}/projects/${PROJECT_ID}

echo Using project URL: ${PROJECT_URL} 

# Require a list of all the merge request and take a look if there is already
# one with the same source branch
LISTMR=`curl --silent "${PROJECT_URL}/merge_requests?state=opened" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}"`;
COUNTBRANCHES=`echo ${LISTMR} | grep -o "\"source_branch\":\"${BRANCH}\"" | wc -l`;

# No MR found, let's create a new one
if [ ${COUNTBRANCHES} -eq "0" ]; then
    curl -i --request POST "${PROJECT_URL}/merge_requests" --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" --header "Content-Type: application/json" --data "{ \"id\": \"${PROJECT_ID}\", \"source_branch\": \"${BRANCH}\", \"target_branch\": \"${TARGET_BRANCH}\", \"remove_source_branch\": \"True\", \"title\": \"Dependency versions update\"}";

    echo "Opened a new merge request to update dependency versions: ${BRANCH}";
    exit;
fi

echo "No new merge request opened";

